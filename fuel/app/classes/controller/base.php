<?php
class Controller_Base extends Controller_template{
  private $current_user;

  public function before(){
    parent::before();
    $this->check_csrf();
    $this->verify_auth();

    $this->template->is_logged_in = $this->is_logged_in();
    $this->template->current_user = $this->current_user();
  }

  public function after($response)
  {
    $response = parent::after($response);

    // Security: クリックジャッキング対策
    $response->set_header('X-FRAME-OPTIONS', 'SAMEORIGIN');
    return $response;
  }

  protected function current_user(){
    return $this->current_user;
  }

  protected function is_logged_in(){
    return isset($this->current_user);
  }

  private function check_csrf(){
    if (Input::method() == 'POST') {
      // Security: CSRF対策
      if(!Security::check_token()){
        Session::set_flash('error', 'csrf token invalid');
        Response::redirect('/');
      }
    }
  }

  private function verify_auth(){
    // Security: セッションハイジャック対応
    $user_id = Session::get('user_id');
    if(!isset($user_id)){
      return;
    }
    $this->current_user = Model_User::find($user_id);
  }
}
