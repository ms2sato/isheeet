<?php
class Util_ValidationRules {
  public static function _validation_password($val) {
    return preg_match("/^[a-zA-Z0-9_]+$/", $val) == 1;
  }
}
